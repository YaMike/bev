#!/usr/bin/env python
#
from BirdsEyeView import BirdsEyeView
from glob import glob
import os,sys
import cv2

def main(dataFiles, pathToCalib, outputPath, isReverse, calib_end  = '.txt'):
    '''
    Main method of transform2BEV
    :param dataFiles: the files you want to transform to BirdsEyeView, e.g., /home/elvis/kitti_road/data/*.png
    :param pathToCalib: containing calib data as txt-files, e.g., /home/elvis/kitti_road/calib/
    :param outputPath: where the BirdsEyeView data will be saved, e.g., /home/elvis/kitti_road/data_bev
    :param calib_end: file extension of calib-files (OPTIONAL)
    '''

    pathToData = os.path.split(dataFiles)[0]
    assert os.path.isdir(pathToData), "The directory containig the input data seems to not exist!"
    assert os.path.isdir(pathToCalib), "Error <PathToCalib> does not exist"
      
    bev = BirdsEyeView()
    
    if not os.path.isdir(outputPath):
        os.makedirs(outputPath)  
    
    fileList_data = glob(dataFiles)
    assert len(fileList_data), 'Could not find files in: %s' %pathToData
    
    for aFile in fileList_data:
        assert os.path.isfile(aFile), '%s is not a file' %aFile
        
        file_key = aFile.split('/')[-1].split('.')[0]
        print "Transforming file %s to Birds Eye View " %file_key 
        tags = file_key.split('_')
        data_end = aFile.split(file_key)[-1]
        
        calib_file = os.path.join(pathToCalib, "config" + calib_end)
        
        if not os.path.isfile(calib_file) and len(tags)==3:
            # exclude lane or road from filename!
            calib_file = os.path.join(pathToCalib, tags[0]+ '_' + tags[2] + calib_end)
        
        if not os.path.isfile(calib_file):
            print "Cannot find calib file: %s" %calib_file
            sys.exit(1)
            
        bev.setup(calib_file)
        
        data = cv2.imread(aFile, cv2.IMREAD_UNCHANGED)
        
        if (isReverse == "dir"):
            data_bev = bev.compute(data)
        elif (isReverse == "rev"):
			data_bev = bev.compute_reverse(data, (400, 800))
        else:
		    print "Unknown transform type"
		    exit()
		    
        fn_out = os.path.join(outputPath,file_key + data_end)
        if (cv2.imwrite(fn_out, data_bev)):
            print "done ..."
        else:
            print "saving to %s failed ... (permissions?)"%outputPath
            return
       
    print "BirdsEyeView was stored in: %s" %outputPath
         
if __name__ == "__main__":
    
    dataFiles = sys.argv[1]
    pathToCalib = sys.argv[2]
    outputPath = sys.argv[3]
    
    isReverse = sys.argv[4]
    
    main(dataFiles, pathToCalib, outputPath, isReverse)
    
