import os
from os import listdir
from os.path import isfile, join

import numpy as np
import sys
import cv2

def main():
    diffEps = 0.5 # pixels percent which can differ
    allowedPixDiff = 5 # pixel's color on one channel can differ on 5
	
    selfPath = os.path.abspath(os.path.dirname(sys.argv[0]))
    sep = "/" # TODO will not work on windows
    
    cppBevScriptPath = selfPath + sep + "computeBevOnCollectionCpp"
    pyBevScriptPath = selfPath + sep + "computeBevOnCollectionPy"
    
    dirTrCppPath = selfPath + sep + "directTransformedCpp"
    revTrCppPath = selfPath + sep + "reverseTransformedCpp"
    dirTrPyPath = selfPath + sep + "directTransformedPython"
    revTrPyPath = selfPath + sep + "reverseTransformedPython"
    
    directCollPath = selfPath + sep + "testCollectionDirect"
    reverseCollPath = selfPath + sep + "testCollectionReverse"
    
    print "-----GENERATING BEV IMAGES USING CPP-----"
    os.system(cppBevScriptPath)
    print "-----------------DONE--------------------"
    print "-----GENERATING BEV IMAGES USING PY-----"
    os.system(pyBevScriptPath)
    print "-----------------DONE-------------------"
    
    imageNamesDirect = [ f for f in listdir(directCollPath) if isfile(join(directCollPath,f)) ]
    imageNamesReverse = [ f for f in listdir(reverseCollPath) if isfile(join(reverseCollPath,f)) ]

    print "-----STARTING DIFF TESTS-----"
    for imageName in imageNamesDirect:
        cppResultPath = dirTrCppPath + sep + imageName
        pyResultPath = dirTrPyPath + sep + imageName
        dataCpp = cv2.imread(cppResultPath, cv2.IMREAD_UNCHANGED)
        dataPy = cv2.imread(pyResultPath, cv2.IMREAD_UNCHANGED)
        
        if (dataCpp.shape != dataPy.shape):
            print "FAILED: %s" % (imageName,)
            continue

        substituted = cv2.absdiff(dataCpp, dataPy)
        fsub = substituted.flatten()
        
        diffPercentage = float(fsub[fsub > allowedPixDiff].size) / dataCpp.size * 100
        print "DIFFERENCE: %s(percent)" % (diffPercentage,)
        
        if (diffPercentage > diffEps):
			print "FAILED: %s" % (imageName,)
        else:
			print "PASSED: %s" % (imageName,)
    print "------------DONE------------"

if __name__ == "__main__":
    main()
