#include <iostream>
#include <string>
#include <memory>

#include <cstdlib>

#include <opencv2/opencv.hpp>

#include "inc/ConfigParser.h"
#include "inc/Params.h"

#include "inc/DirectTransform.h"
#include "inc/ReverseTransform.h"

void usage(char *argv[])
{
	std::cout << "Usage: " << std::string(argv[0]);
	std::cout << " path/to/config path/to/input/image path/to/output/image rev|dir" << std::endl;
	exit(1);
}

int main(int argc, char* argv[])
{
    if (argc != 5)
    {
        std::cerr << "Invalid input arguments count" << std::endl;
		usage(argv);
        return 1;
    }

    bev::ConfigParser parser;
    auto config = parser.parse(argv[1]);
    auto image = cv::imread(argv[2], CV_LOAD_IMAGE_UNCHANGED);

    std::shared_ptr<bev::Transform> tr = nullptr;

    if (std::string(argv[4]) == "rev")
    {
        tr = std::move(std::shared_ptr<bev::Transform>(new bev::ReverseTransform(config, std::make_pair(800, 400))));
    }
    else if (std::string(argv[4]) == "dir")
    {
        tr = std::move(std::shared_ptr<bev::Transform>(new bev::DirectTransform(config)));
    }
    else
    {
        std::cout << "Invalid transform type requested" << std::endl;
        return 1;
    }

    auto result = tr->compute(image);

    try
    {
        std::cout << "Saving to : " << std::string(argv[3]) << std::endl;
        cv::imwrite(argv[3], result);
    }
    catch (std::runtime_error& ex)
    {
        std::cout << "Unable to save image. Details: " << std::string(ex.what()) << std::endl;
    }

    return 0;
}

