#pragma once

#include <cstdio>
#include <utility>

namespace bev
{
	const double defaultTransformRes = 0.05;
	const std::pair<double, double> default_xLimits = std::make_pair<const double, const double>(-10, 10);
	const std::pair<double, double> default_zLimits = std::make_pair<const double, const double>(6, 46);

	struct Params
	{
		public:
			const double transformRes;
			const std::pair<const double, const double> xLimits;
			const std::pair<const double, const double> zLimits;
			const std::pair<const size_t, const size_t> transformSize;

			Params(const double resolution,
				const std::pair<const double, const double> xLimits,
				const std::pair<const double, const double> zLimits);

			Params(): Params(defaultTransformRes, default_xLimits, default_zLimits) {
			}
	};
}
