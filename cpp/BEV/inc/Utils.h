#pragma once

#include <cstdio>
#include <utility>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/io.hpp>

// noexcept is not defined under vs... Damn microsoft!
#ifndef _MSC_VER
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif

namespace bev
{
namespace utils
{
    using namespace boost::numeric::ublas;
	namespace ublas = boost::numeric::ublas;

    using ulong = unsigned long;

    template<typename V, typename U>
    std::pair<ublas::vector<U>, ublas::vector<U> > select(const ublas::vector<V>& validationArrX,
			    const ublas::vector<V>& validationArrY,
			    const ublas::vector<U>& valueArrX,
			    const ublas::vector<U>& valueArrY,
			    const std::pair<const size_t, const size_t> imSize) NOEXCEPT
    {
        assert(validationArrX.size() == validationArrY.size()
               && validationArrX.size() == valueArrX.size()
               && valueArrX.size() == valueArrY.size());

		std::pair<ublas::vector<U>, ublas::vector<U> > result(ublas::vector<U>(valueArrX.size()),
																ublas::vector<U>(valueArrY.size()));

		typename ublas::vector<V>::size_type validIndex = 0;

		for (typename ublas::vector<V>::size_type j = 0; j < valueArrX.size(); ++j)
        {
            if (validationArrX(j) >= 1 && validationArrX(j) <= imSize.second
                && validationArrY(j) >= 1 && validationArrY(j) <= imSize.first)
            {
                result.first(validIndex) = valueArrX(j);
                result.second(validIndex++) = valueArrY(j);
            }
        }

        result.first.resize(validIndex);
        result.second.resize(validIndex);

        return std::move(result);
    }
} // utils
} // bev
