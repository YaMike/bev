#pragma once

#include "inc/Transform.h"
#include "inc/GPUComp.h"

namespace bev
{
    class DirectTransform : public Transform
    {
    private:
        vector<uint32_t> bevXIndicies;
        vector<uint32_t> bevZIndicies;
        vector<double> uMat;
        vector<double> vMat;
        std::pair<size_t, size_t> inputImageSize;

	matrix<double>
	computeUvMat(const size_t hei, const size_t wid) const NOEXCEPT;

	matrix<double>
	world2image(const matrix<double>& uvMat) const NOEXCEPT;

    protected:
        void computeLookUpTable() NOEXCEPT override;

    public:
        DirectTransform(const ParsedConfig& cnfg)
            : Transform(cnfg) {}

        virtual image compute(const image& input);
    };
} // bev
