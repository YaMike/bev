#pragma once

#include <opencv2/opencv.hpp>

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

cudaError_t GPUCompDirectTrfKern(const cv::Mat &inMat,
						const boost::numeric::ublas::vector<uint32_t> &bevXIdxsVec,
						const boost::numeric::ublas::vector<uint32_t> &bevZIdxsVec,
						const boost::numeric::ublas::vector<uint32_t> &uVec,
						const boost::numeric::ublas::vector<uint32_t> &vVec,
						cv::Mat &outMat);

cudaError_t GPUCompReverseTrfKern(const cv::Mat &inMat,
						const boost::numeric::ublas::vector<uint32_t> &bevXIdxsVec,
						const boost::numeric::ublas::vector<uint32_t> &bevZIdxsVec,
						const boost::numeric::ublas::vector<uint32_t> &uVec,
						const boost::numeric::ublas::vector<uint32_t> &vVec,
						cv::Mat &outMat);

