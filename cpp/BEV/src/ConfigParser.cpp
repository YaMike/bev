#include <string>
#include <fstream>
#include <sstream>
#include <iterator>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

#include "inc/Exceptions.h"
#include "inc/ConfigParser.h"

using namespace std;
using namespace bev;
using namespace boost::numeric::ublas;

ParsedConfig::ParsedConfig() : P0(3, 4), P1(3, 4), P2(3, 4),
                               R0(3, 3), TrVeloToCam(3, 4),
                               TrImuToVelo(3, 4), TrCamToRoad(3, 4)
{}

ParsedConfig::ParsedConfig(const ParsedConfig& other)
{
    if (this == &other) { return; }

    this->P0 = other.P0;
    this->P1 = other.P1;
    this->P2 = other.P2;
    this->R0 = other.R0;
    this->TrCamToRoad = other.TrCamToRoad;
    this->TrImuToVelo = other.TrImuToVelo;
    this->TrVeloToCam = other.TrVeloToCam;
}

ParsedConfig& ParsedConfig::operator = (const ParsedConfig& other)
{
    if (this == &other) { return * this; }

    this->P0 = other.P0;
    this->P1 = other.P1;
    this->P2 = other.P2;
    this->R0 = other.R0;
    this->TrCamToRoad = other.TrCamToRoad;
    this->TrImuToVelo = other.TrImuToVelo;
    this->TrVeloToCam = other.TrVeloToCam;

    return *this;
}

ParsedConfig::ParsedConfig(ParsedConfig&& other)
{
    if (this == &other) { return; }

    this->P0 = move(other.P0);
    this->P1 = move(other.P1);
    this->P2 = move(other.P2);
    this->R0 = move(other.R0);
    this->TrCamToRoad = move(other.TrCamToRoad);
    this->TrImuToVelo = move(other.TrImuToVelo);
    this->TrVeloToCam = move(other.TrVeloToCam);
}

ParsedConfig& ParsedConfig::operator = (ParsedConfig&& other)
{
    if (this == &other) { return *this; }

    this->P0 = move(other.P0);
    this->P1 = move(other.P1);
    this->P2 = move(other.P2);
    this->R0 = move(other.R0);
    this->TrCamToRoad = move(other.TrCamToRoad);
    this->TrImuToVelo = move(other.TrImuToVelo);
    this->TrVeloToCam = move(other.TrVeloToCam);

    return *this;
}

matrix<double> ConfigParser::getMatrixByJsonKey(const boost::property_tree::ptree& pt,
                                                const string& key,
                                                const matrix<double>::size_type rows,
                                                const matrix<double>::size_type cols) const
{
    matrix<double> res(rows, cols);
    int i = 0;

    for (auto& rowPair : pt.get_child(key))
    {
        int j = 0;
        for (auto& itemPair : rowPair.second)
        {
            res(i, j++) = itemPair.second.get_value<double>();
        }
        ++i;
    }

    return move(res);
}

ParsedConfig ConfigParser::parse(const string& configName) const
{
    ParsedConfig result;

    stringstream ss;

    ifstream jsonConfig(configName);
    if (!jsonConfig.is_open())
    {
        throw no_such_file_exception("Could not open " + configName);
    }

    while(jsonConfig >> ss.rdbuf());
    jsonConfig.close();

    boost::property_tree::ptree pt;

    try
    {
        boost::property_tree::read_json(ss, pt);
    }
    catch (boost::property_tree::json_parser::json_parser_error& e)
    {
        throw invalid_config_format_exception("Invalid config format. Details: " + string(e.what()));
    }

    try
    {
        result.P0 = getMatrixByJsonKey(pt, p0_key, result.P0.size1(), result.P0.size2());
        result.P1 = getMatrixByJsonKey(pt, p1_key, result.P1.size1(), result.P1.size2());
        result.P2 = getMatrixByJsonKey(pt, p2_key, result.P2.size1(), result.P2.size2());
        result.R0 = getMatrixByJsonKey(pt, r0_key, result.R0.size1(), result.R0.size2());
        result.TrCamToRoad = getMatrixByJsonKey(pt, trC2RKey, result.TrCamToRoad.size1(), result.TrCamToRoad.size2());
        result.TrImuToVelo = getMatrixByJsonKey(pt, trI2VKey, result.TrImuToVelo.size1(), result.TrImuToVelo.size2());
        result.TrVeloToCam = getMatrixByJsonKey(pt, trV2CamKey, result.TrVeloToCam.size1(), result.TrVeloToCam.size2());
    }
    catch (boost::property_tree::ptree_bad_path& e)
    {
        throw json_node_does_not_exist("Key is missing in config. Details: " + string(e.what()));
    }

    return move(result);
}

