#include <GPUComp.h>

#include <opencv2/opencv.hpp>

#include <cstdint>
#include <cstring>
#include <cassert>

/*
 * Execute each (u,v) pair in a separate thread
 */
__global__ void direct_kernel(double *inMat, int inMat_height, int inMat_width, int inMat_channels,
							  uint32_t *bevXIdxs,
							  uint32_t *bevZIdxs,
							  uint32_t *uVec,
							  uint32_t *vVec,
							  uint32_t size,
							  double *outMat)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x; /* composite index  for (u,v) pair */
	if (idx > size) {
		return;
	}

	int bevZVal = bevZIdxs[idx] - 1;
	int bevXVal = bevXIdxs[idx] - 1;
	int uMatVal = uVec[idx] - 1;
	int vMatVal = vVec[idx] - 1;

	for (int i = 0; i < inMat_channels; i++) {
		outMat[bevZVal * inMat_width * inMat_channels + bevXVal * inMat_channels + i] = inMat[vMatVal * inMat_width * inMat_channels + uMatVal * inMat_channels + i];
	}
}

cudaError_t GPUCompDirectTrfKern(const cv::Mat &inMat,
						const boost::numeric::ublas::vector<uint32_t> &bevXIdxsVec,
						const boost::numeric::ublas::vector<uint32_t> &bevZIdxsVec,
						const boost::numeric::ublas::vector<uint32_t> &uVec,
						const boost::numeric::ublas::vector<uint32_t> &vVec,
						cv::Mat &outMat)
{
	uint32_t *dev_bevXIdxs = NULL;
	uint32_t *dev_bevZIdxs = NULL;
	uint32_t *dev_uVec     = NULL;
	uint32_t *dev_vVec     = NULL;
	double *dev_inMat    = NULL;
	double *dev_outMat   = NULL;

	cudaError_t err = cudaSuccess;

	const int threads_count = 256;
	const int blocks_count = (inMat.total() * inMat.channels() + threads_count - 1)/threads_count;

	assert(bevXIdxsVec.size() == bevZIdxsVec.size() && uVec.size() == vVec.size() && bevXIdxsVec.size() == uVec.size());
	int size = uVec.size();

	/* 
	 * FIXME:
	 * allocation/freeing can be moved outside this method when optimization will be required.
	 */
	if (cudaSuccess != (err = cudaMalloc(&dev_bevXIdxs, bevXIdxsVec.size() * sizeof(*dev_bevXIdxs)))
			|| cudaSuccess != (err = cudaMalloc(&dev_bevZIdxs, bevZIdxsVec.size() * sizeof(*dev_bevZIdxs)))
			|| cudaSuccess != (err = cudaMalloc(&dev_uVec, uVec.size() * sizeof(*dev_uVec)))
			|| cudaSuccess != (err = cudaMalloc(&dev_vVec, vVec.size() * sizeof(*dev_vVec)))
			|| cudaSuccess != (err = cudaMalloc(&dev_inMat, inMat.total() * inMat.channels() * sizeof(*dev_inMat)))
			|| cudaSuccess != (err = cudaMalloc(&dev_outMat, outMat.total() * outMat.channels() * sizeof(*dev_outMat)))) {
		goto dfinish;
	}

	/* copy input vectors to device memory */
	if (cudaSuccess != (err = cudaMemcpy(dev_bevXIdxs, &bevXIdxsVec.data()[0], bevXIdxsVec.size() * sizeof(*dev_bevXIdxs), cudaMemcpyHostToDevice))
			|| cudaSuccess != (err = cudaMemcpy(dev_bevZIdxs, &bevZIdxsVec.data()[0], bevZIdxsVec.size() * sizeof(*dev_bevZIdxs), cudaMemcpyHostToDevice))
			|| cudaSuccess != (err = cudaMemcpy(dev_uVec, &uVec.data()[0], uVec.size() * sizeof(*dev_uVec), cudaMemcpyHostToDevice))
			|| cudaSuccess != (err = cudaMemcpy(dev_vVec, &vVec.data()[0], vVec.size() * sizeof(*dev_vVec), cudaMemcpyHostToDevice))) {
		goto dfinish;
	}

	/* transfer inMat to device */
	if (inMat.isContinuous()) {
		const double *sptr = inMat.ptr<double>(0);
		if (cudaSuccess != (err = cudaMemcpy(dev_inMat, sptr, inMat.total() * inMat.channels(), cudaMemcpyHostToDevice))) {
			goto dfinish;
		}
	} else {
		const cv::Size inSize(inMat.size());
		boost::numeric::ublas::vector<double> inMatFlat(inSize.height * inSize.width * inMat.channels());
		double *dptr = &inMatFlat.data()[0];

		for (int i = 0; i < inSize.height; ++i) {
			const double *sptr = inMat.ptr<double>(i);
			std::memcpy(dptr, sptr, inSize.width * inMat.channels());
			dptr += inSize.width * inMat.channels();
		}

		if (cudaSuccess != (err = cudaMemcpy(dev_inMat, &inMatFlat.data()[0], inMatFlat.size(), cudaMemcpyHostToDevice))) {
			goto dfinish;
		}
	}

	/*
     * FIXME:
	 * Algorithm generating transformation data should generate only one matrix with two coordinates and channel number.
	 * No need to use 4 vectors for convertation.
	 */

	direct_kernel<<<blocks_count, threads_count>>>(dev_inMat, inMat.size().height, inMat.size().width, inMat.channels(),
													dev_bevXIdxs,
													dev_bevZIdxs,
													dev_uVec,
													dev_vVec,
													vVec.size(),
													dev_outMat);

	/* Convert result to Mat */
	if (outMat.isContinuous()) {
		const cv::Size inSize(inMat.size());
		double *dptr = outMat.ptr<double>(0);
		if (cudaSuccess != (err = cudaMemcpy(dptr, dev_outMat, inSize.height * inSize.width * outMat.channels(), cudaMemcpyDeviceToHost))) {
			goto dfinish;
		}
	} else {
		/* Transfer result to host */
		const cv::Size inSize(inMat.size());
		boost::numeric::ublas::vector<double> outMatFlat(inSize.height * inSize.width * inMat.channels());
		double *dptr = &outMatFlat.data()[0];

		if (cudaSuccess != (err = cudaMemcpy(dptr, dev_outMat, inSize.height * inSize.width * outMat.channels(), cudaMemcpyDeviceToHost))) {
			goto dfinish;
		}

		const double *sptr = &outMatFlat.data()[0];

		for (int i = 0; i < inSize.height; ++i) {
			double *dptr = outMat.ptr<double>(i);
			std::memcpy(dptr, sptr, inSize.width * inMat.channels());
			sptr += inSize.width * inMat.channels();
		}
	}

dfinish:
	cudaFree(dev_bevXIdxs);
	cudaFree(dev_bevZIdxs);
	cudaFree(dev_uVec);
	cudaFree(dev_vVec);
	cudaFree(dev_inMat);

	return err;
}

__global__ void reverse_kernel(double *inMat, int inMat_height, int inMat_width, int inMat_channels,
							  uint32_t *bevXIdxs,
							  uint32_t *bevZIdxs,
							  uint32_t *uVec,
							  uint32_t *vVec,
							  uint32_t vVec_size,
							  double *outMat)
{
	return;
}

cudaError_t GPUCompReverseTrfKern(const cv::Mat &inMat,
						boost::numeric::ublas::vector<uint64_t> &bevXIdxsVec,
						boost::numeric::ublas::vector<uint64_t> &bevZIdxsVec,
						boost::numeric::ublas::vector<double> &uVec,
						boost::numeric::ublas::vector<double> &vVec,
						cv::Mat &outMat)
{
		return cudaSuccess;
}

