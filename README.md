# README #
![Birds_Eye_View_logo.jpg](https://bitbucket.org/repo/qqngnk/images/966658454-Birds_Eye_View_logo.jpg)
### What is this repository for? ###
This repo implements Birds Eye Transform in C++.  
Also contains original python version and tests.  
Successful builds were obtained on Debian Jessie + gcc 4.9 and Windows 8.1 + visual studio 2013.  
Of course you are free to build via other tools if you like.

### How do I get set up? ###
#### Summary of set up ####
##### Linux #####
* Install cmake  
* Install or build opencv  
* Build boost  
* clone bev project  
* cd to bev source dir  
* mkdir build  
* cd build/  
* cmake ..  
* make  
That's it. You are done.

##### Windows#####
* Install cmake, add it to Path  
* Make sure that you have Visual Studio at least 2013  
You will not be able to build otherwise  
* Download boost 1.59 and follow official instruction to build it.
Bev be much more effecient if you will build boost with C++ 11 support.
Make sure that you have BOOST_ROOT variable set  
* Download opencv 3.0.0 and follow official instruction
(http://docs.opencv.org/3.0-beta/doc/tutorials/introduction/windows_install/windows_install.html#windowssetpathandenviromentvariable)  
Make sure that you have set all variables correctly!
* clone bev from this repo
* cd to bev dir
* mkdir (md? oh... windows...) build
* cmake -G "Visual Studio 2013" ..
* Open generated solution
* Add opencv include to Include directories for Bev project
* Make sure you have Configuration Properties->C/C++->Code generation->Runtime Library->/MTd set for Bev project.
* Click build and be happy.

#### Dependencies ####
boost (tested on 1.59)  
opencv (tested on 3.0.0)
#### How to run tests ####
Tests are only available under linux.
Just run python BevTests/testBySubstution.py
This test substracts images obtained by cpp and python version.  
If difference is small then test marks as PASSED.

### Who do I talk to? ###
Contacts:  
Skype: vladimir.tsyshnatiy  
Email: vtsyshnatiy@gmail.com  